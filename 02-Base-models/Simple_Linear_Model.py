import tensorflow as tf
import os
from log.graph_visualization import GRAPH_LOG_DIR
graph_log = os.path.join(GRAPH_LOG_DIR, 'simple_linear_model')

# build graph part
#Model definition

W = tf.Variable([.3], tf.float32)
b = tf.Variable([-.3], tf.float32)
x = tf.placeholder(tf.float32)

init = tf.global_variables_initializer()

linear_model = W * x + b

# Calculate the loss
y = tf.placeholder(tf.float32) # the desired values , to evaluate the model on training

def mean_square_error():
    #Mean square error
    square_deltas = tf.square(linear_model - y) # square of prediction - real value
    mse_loss = tf.reduce_sum(square_deltas)
    return mse_loss

#Train process
loss_function = mean_square_error()
# Optimizer
# Gradient Descent
learning_rate = 0.001
sgd_optimizer = tf.train.GradientDescentOptimizer(learning_rate)

train = sgd_optimizer.minimize(loss_function) # function to minimize


with tf.Session() as sess:
    graph_writer = tf.summary.FileWriter(graph_log, sess.graph)

    #initialize variables
    sess.run(init)
    for i in range(1000):
        output, loss , _ = sess.run([linear_model, mean_square_error(), train], feed_dict={x: [1, 2, 3, 4, 5], y: [0.1, 0.2, 0.3, 0.4, 0.5]})
        if i % 100 == 0:
            print("output=", output, ", loss=",loss)

    weights, bias = sess.run([W, b])
    print("model weights : ", weights)
    print("model bias : ", bias)

    sess.close()