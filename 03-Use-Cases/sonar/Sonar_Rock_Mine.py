import matplotlib.pyplot as plt
import tensorflow as tf
import numpy as np
import pandas as pd
from sklearn.preprocessing import LabelEncoder
from sklearn.utils import shuffle
from sklearn.model_selection import train_test_split
from data import DATA_DIR
from models.sonar import SONAR_MODEL_DIR
from log.graph_visualization import GRAPH_LOG_DIR
import os



# Encode function
def one_hot_encoder(labels):
    n_labels = len(labels)
    n_unique_labels = len(np.unique(labels))
    one_hot_encode = np.zeros((n_labels, n_unique_labels))
    one_hot_encode[np.arange(n_labels), labels] = 1
    return one_hot_encode

#Preprocess dataset
#Read dataset
def read_dataset():
    df = pd.read_csv(os.path.join(DATA_DIR, 'sonar.csv'))

    X = df[df.columns[0:60]].values #features
    y = df[df.columns[60]] # labels (last columns)

    #Encode labels
    encoder = LabelEncoder()
    encoder.fit(y)

    y = encoder.transform(y)
    Y = one_hot_encoder(y)

    return (X, Y)

# Read dataset
X, Y = read_dataset()

#Shuffle dataset
X, Y = shuffle(X, Y, random_state=1)

#Convert dataset into train data and test data
train_x, test_x, train_y, test_y = train_test_split(X, Y, test_size=0.20, random_state=415)

print(train_x.shape)
print(train_y.shape)
print(test_x.shape)
print(test_y.shape)

#define parameters
learning_rate = 0.3
epochs = 1000
cost_history = np.empty(shape=[1], dtype=float)
n_dim = X.shape[1]
n_class = 2

# Hyperparameters hidden layer definition
n_hidden_1 = 120
n_hidden_2 = 120
n_hidden_3 = 120
n_hidden_4 = 120

model_name = 'MLP-' + str(n_hidden_1) + \
                        '-'+ str(n_hidden_1) + \
                          '-'+ str(n_hidden_1) + \
                          '-'+ str(n_hidden_1) + '-lr-' + str(learning_rate)

model_path = os.path.join(SONAR_MODEL_DIR, model_name)

graph_log = os.path.join(GRAPH_LOG_DIR, 'Sonar_Rock_Mine', model_name)


#Compute elements
x = tf.placeholder(tf.float32, [None, n_dim])
y = tf.placeholder(tf.float32, [None, n_class])




#define model
def multilayer_perceptron(x, weights, biases):

    #hidden layer definition sigmoid activation
    with tf.name_scope('layer1'):
        layer_1 = tf.add(tf.matmul(x, weights['h1']), biases['b1'])
        layer_1 = tf.nn.sigmoid(layer_1)

    with tf.name_scope('layer2'):
        layer_2 = tf.add(tf.matmul(layer_1, weights['h2']), biases['b2'])
        layer_2 = tf.nn.sigmoid(layer_2)

    with tf.name_scope('layer3'):
        layer_3 = tf.add(tf.matmul(layer_2, weights['h3']), biases['b3'])
        layer_3 = tf.nn.sigmoid(layer_3)

    with tf.name_scope('layer4'):
        layer_4 = tf.add(tf.matmul(layer_3, weights['h4']), biases['b4'])
        layer_4 = tf.nn.sigmoid(layer_4)

    #Output layer
    output_layer = tf.matmul(layer_4, weights['out']) + biases['out']
    return output_layer


#Define weights and biases for each layer

weights = {
    'h1': tf.Variable(tf.truncated_normal([n_dim, n_hidden_1]), "w_h_1"),
    'h2': tf.Variable(tf.truncated_normal([n_hidden_1, n_hidden_2]), "w_h_2"),
    'h3': tf.Variable(tf.truncated_normal([n_hidden_2, n_hidden_3]), "w_h_3"),
    'h4': tf.Variable(tf.truncated_normal([n_hidden_3, n_hidden_4]), "w_h_4"),
    'out': tf.Variable(tf.truncated_normal([n_hidden_4, n_class]), "w_out"),
}
# To see weights distribution
histogram_w_h1 = tf.summary.histogram('weights_hidden_1', weights['h1'])
histogram_w_h2 = tf.summary.histogram('weights_hidden_2', weights['h2'])
histogram_w_h3 = tf.summary.histogram('weights_hidden_3', weights['h3'])
histogram_w_h4 = tf.summary.histogram('weights_hidden_4', weights['h4'])
histogram_w_h5 = tf.summary.histogram('weights_out', weights['out'])

biases = {
    'b1': tf.Variable(tf.truncated_normal([n_hidden_1])),
    'b2': tf.Variable(tf.truncated_normal([n_hidden_2])),
    'b3': tf.Variable(tf.truncated_normal([n_hidden_3])),
    'b4': tf.Variable(tf.truncated_normal([n_hidden_4])),
    'out': tf.Variable(tf.truncated_normal([n_class]))
}

#Initialize Variables
init = tf.global_variables_initializer()

output = multilayer_perceptron(x, weights, biases)

#Define cost function and optimizer
with tf.name_scope('loss'):
    cost_function = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(logits=output, labels=y))
    optimizer = tf.train.GradientDescentOptimizer(learning_rate).minimize(cost_function)
    t_loss_summary = tf.summary.scalar("train_loss", cost_function)
    val_loss_summary = tf.summary.scalar("val_loss", cost_function)

with tf.name_scope('accuracy'):
    correct_prediction = tf.equal(tf.argmax(output, 1), tf.argmax(y, 1))
    accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))
    t_acc_summary = tf.summary.scalar("train_accuracy", accuracy)
    v_acc_summary = tf.summary.scalar("val_accuracy", accuracy)

saver = tf.train.Saver()
all_summaries = tf.summary.merge_all()


with tf.Session() as sess:
    graph_writer = tf.summary.FileWriter(graph_log, sess.graph)
    sess.run(init)

    mse_history = []
    accuracy_history = []

    #Accuracy compute functions
    correct_prediction = tf.equal(tf.argmax(output, 1), tf.argmax(y, 1))
    accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))

    for epoch in range(epochs):

        cost, acc, _, all_sum = sess.run([cost_function, accuracy, optimizer, all_summaries], feed_dict={x: train_x, y: train_y})

        # add cost value to history
        cost_history = np.append(cost_history, cost)

        pred_y = sess.run(output, feed_dict={x: train_x})
        mean_square_error = tf.reduce_mean(tf.square(pred_y - test_y))

        mse = sess.run(mean_square_error)

        # Compute validation accuracy

        accuracy_history.append(acc)

        graph_writer.add_summary(all_sum, epoch + 1)

        mse_history.append(mse)

        print('epoch : ', epoch, ' - ', 'cost : ', cost, ' - MSE : ', mse, '- train accuracy : ', acc)

    save_path = saver.save(sess, model_path)
    print("Model saved in file: %s" % save_path)

    #plot mse and accuracy
    plt.plot(mse_history, 'r')
    plt.show()
    plt.plot(accuracy_history)
    plt.show()

    correct_prediction = tf.equal(tf.argmax(output, 1), tf.argmax(y, 1))
    accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))

    val_acc = sess.run(accuracy, feed_dict={x: test_x, y: test_y})
    print('validation accuracy : ', val_acc)

    pred_y = sess.run(output, feed_dict={x: test_x})
    mean_square_error = tf.reduce_mean(tf.square(pred_y - test_y))

    final_mse = sess.run(mean_square_error)
    print('MSE: %.4f' % final_mse)
