# Variables : to accept external inputs that can change through process

import tensorflow as tf
from log.graph_visualization import GRAPH_LOG_DIR
import os
graph_log = os.path.join(GRAPH_LOG_DIR, 'linear_model_compute_graph')

W = tf.Variable([.3], tf.float32)
b = tf.Variable([.3], tf.float32)
x = tf.placeholder(tf.float32) # Empty for now but we set the type of values

linear_model = W * x + b
#Initialize variable
init = tf.global_variables_initializer()

with tf.Session() as sess:

    sess.run(init) # run variables initializer

    graph_writer = tf.summary.FileWriter(graph_log, sess.graph)
    # we feed the placeholders with external values
    # And compute linear model
    output = sess.run(linear_model, feed_dict={x: [1, 2, 3, 4]})
    print(output)