# Placeholder : to accept external inputs

import tensorflow as tf
from log.graph_visualization import GRAPH_LOG_DIR
import os
graph_log = os.path.join(GRAPH_LOG_DIR, 'array_compute_graph')

a = tf.placeholder(tf.float32) # Empty for now but we set the type of values
b = tf.placeholder(tf.float32)

adder_node = a + b

with tf.Session() as sess:

    graph_writer = tf.summary.FileWriter(graph_log, sess.graph)
    # we feed the placeholders with external values
    output = sess.run(adder_node, feed_dict={a:[1, 3], b:[4, 5]})
    print(output)