import tensorflow as tf

#build computable graph
node1 = tf.constant(3.0, tf.float32)
node2 = tf.constant(4.0)

node3 = node1 * node2
#print(node1, node2) # Tensor("Const:0", shape=(), dtype=float32) Tensor("Const_1:0", shape=(), dtype=float32)
# Just an extract tensor

# Execute the graph

#sess = tf.Session()
#print(sess.run([node1, node2]))

#free session
#sess.close()

with tf.Session() as sess:
    # run graph elements
    output = sess.run(node3)
    print(output)

    sess.close()