import tensorflow as tf
from log.graph_visualization import GRAPH_LOG_DIR
import os

log_simple_graph = os.path.join(GRAPH_LOG_DIR, 'simple_graph')

node1 = tf.constant(3.0)
node2 = tf.constant(4.0)

node3 = node1 * node2



with tf.Session() as sess:
    #Set summary writer for graph compute
    file_writer = tf.summary.FileWriter(log_simple_graph, sess.graph)


    output = sess.run(node3)
    print(output)

    sess.close()